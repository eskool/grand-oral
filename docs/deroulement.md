# Déroulement du Grand Oral (Voie Générale)

Page Officielle de Référence : [ici](https://www.education.gouv.fr/bo/21/Hebdo31/MENE2121378N.htm)

## DÉFINITION ET OBJECTIFS

* Il s'agit d'une Épreuve orale
* Durée : $20$ minutes
* Préparation : $20$ minutes
* Coefficient : $10$

L'épreuve orale terminale est l'une des cinq épreuves terminales de l'examen du baccalauréat.

Elle est obligatoire pour tous les candidats qui présentent l'épreuve dans les mêmes conditions.

Les candidats à besoins éducatifs particuliers peuvent demander à bénéficier d'aménagements de l'épreuve conformément à l'annexe 2.

## FINALITÉ DE L'ÉPREUVE

L'épreuve permet au candidat de montrer sa capacité à prendre la parole en public de façon claire et convaincante. Elle lui permet aussi de mettre les savoirs qu'il a acquis, particulièrement dans ses enseignements de spécialité, au service d'une argumentation, et de montrer comment ces savoirs ont nourri son projet de poursuite d'études, voire son projet professionnel.

## ÉVALUATION DE L'ÉPREUVE

L'épreuve est notée sur 20 points.

Le jury valorise la solidité des connaissances du candidat, sa capacité à argumenter et à relier les savoirs, son esprit critique, la précision de son expression, la clarté de son propos, son engagement dans sa parole, sa force de conviction. Il peut s'appuyer sur la grille indicative de l'annexe 1.

## FORMAT ET DÉROULEMENT DE L'ÉPREUVE

L'épreuve, d'une durée totale de 20 minutes, se déroule en trois temps :

### Premier temps : présentation d'une question ($5$ minutes)

Au début de l'épreuve, le candidat présente au jury deux questions.

Ces questions portent sur les deux enseignements de spécialité soit pris isolément, soit abordés de manière transversale. Elles mettent en lumière un des grands enjeux du ou des programmes de ces enseignements. Elles sont adossées à tout ou partie du programme du cycle terminal. Pour les candidats scolarisés, elles ont été élaborées et préparées par le candidat avec ses professeurs et, s'il le souhaite, avec d'autres élèves.

Les questions sont transmises au jury, par le candidat, sur une feuille signée par les professeurs des enseignements de spécialité du candidat et portant le cachet de son établissement d'origine.

Le jury choisit une des deux questions. Le candidat dispose de 20 minutes de préparation pour mettre en ordre ses idées et réaliser, s'il le souhaite, un support. Ce support ne fait pas l'objet d'une évaluation. Pour son exposé, le candidat dispose du support qu'il a préparé.

Le candidat explique pourquoi il a choisi de préparer cette question pendant sa formation, puis il la développe et y répond. 

Le jury évalue les capacités argumentatives et les qualités oratoires du candidat.

### Deuxième temps : échange avec le candidat ($10$ minutes)

Le jury interroge ensuite le candidat pour l'amener à préciser et à approfondir sa pensée. Il peut interroger le candidat sur toute partie du programme du cycle terminal de ses enseignements de spécialité, en lien avec le premier temps de l'épreuve qui lui-même s'adosse à ces enseignements. Le jury évalue ainsi la solidité des connaissances et les capacités argumentatives du candidat.

### Troisième temps : échange sur le projet d'orientation du candidat ($5$ minutes)

Le candidat explique en quoi la question traitée éclaire son projet de poursuite d'études, voire son projet professionnel. Il expose les différentes étapes de la maturation de son projet (rencontres, engagements, stages, mobilité internationale, intérêt pour les enseignements communs, choix de ses spécialités, etc.) et la manière dont il souhaite le mener après le baccalauréat.

Le jury mesure la capacité du candidat à conduire et exprimer une réflexion personnelle témoignant de sa curiosité intellectuelle et de son aptitude à exprimer ses motivations.

Le candidat effectue sa présentation du premier temps debout, sauf aménagements pour les candidats à besoins spécifiques. Pour les deuxième et troisième temps de l'épreuve, le candidat est assis ou debout selon son choix.

Si la question traitée concerne l'enseignement de spécialité langues, littératures et cultures étrangères et régionales, chacun des deux premiers temps de l'épreuve orale terminale peut se dérouler, en partie, dans la langue vivante concernée par l'enseignement de spécialité, selon le choix du candidat.

## CANDIDATS INFIVIDUELS OU ISSUS DES ÉTABLISSEMENTS PRIVÉS HORS CONTRATS

Les candidats individuels ou les candidats issus des établissements scolaires privés hors contrat présentent l'épreuve orale terminale dans les mêmes conditions que les candidats scolaires. Le document précisant les questions présentées par le candidat à destination du jury est alors constitué par le candidat lui-même, en conformité avec le cadre défini pour les candidats scolaires. 

## COMPOSITION DU JURY

Le jury est composé de deux professeurs de disciplines différentes, dont l'un représente l'un des deux enseignements de spécialité du candidat et l'autre représente un autre enseignement (spécialité ou enseignements communs), ou est professeur-documentaliste. Ils contribuent ensemble à l'évaluation globale du candidat.