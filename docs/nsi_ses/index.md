# Questions Grand Oral NSI - SES

* Les entreprises du numérique : Ubérisation de l’économie ?
* La Blockchain : Quel fonctionnement & quelles Applications en SES et/ou en Mathématiques ?
    * [Introduction au bitcoin et à la blockchain](https://github.com/exo7math/python1-exo7/blob/master/bitcoin/bitcoin.pdf)
    * [Vidéo YouTube](https://www.youtube.com/watch?reload=9&v=SccvFbyDaUI)
* Les cryptomonnaies sont-elles réellement des monnaies ?
    * [France Culture](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco/le-bitcoin-est-il-une-monnaie-3470071)
* Le numérique : Chance ou Risque pour l'économie ?
* Hacking & Cybercriminalité : un risque réel pour les entreprises ?
    * [France Culture](https://www.radiofrance.fr/franceculture/podcasts/serie-l-eco-face-a-la-menace-cyber)
* Internet : un passage obligé pour les entreprises ?
    Quelques émissions ici sur l'économie face aux menaces informatiques sur :
        Dépendance de l’économie à internet
        Hacking, cybercriminalité
        Arnaques sentimentales (il me paraît difficile d'en faire un projet en NSI, en SES également, mais n'étant pas professeur de cette matière, peut-être que oui)

Merci aux collègues de la liste NSI, tout ce qui suit sur ce sujet vient d'eux.
Concernant une implantation simple du mécanisme de la blockchain en Python, il y a cette page, qui n'a pas l'air mal.
Sur les concepts généraux de la blockchain, la vidéo de David Louapre est très bien.