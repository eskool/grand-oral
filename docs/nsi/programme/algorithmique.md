# Questions Grand Oral NSI : Algorithmique

Dans cette partie, beaucoup de questions qui relèvent plus de problèmes sociétaux liés à l'usage de l'informatique, qu'à des algorihtmes absolument inaccessibles en Terminale (comme en bac +1, +2, +3...). Toutes ces questions semblent être compatibles avec les spécialités SES / histoire géographie, géopolitique et sciences politique, et peut-être aussi humanités, littérature et philosophie.

* Comment créer une machine intelligente ?
* Comment lutter contre les biais algorithmiques ?
* Quels sont les enjeux de la reconnaissance faciale (notamment éthiques) ?
* Quels sont les enjeux de l’intelligence artificielle ?
* Transformation d’images : Deep Fakes, une arme de désinformation massive ? La fin
de la preuve par l’image ?
* Qu’apporte la récursivité dans un algorithme ?
* Quel est l’impact de la complexité d’un algorithme sur son efficacité ?
* Comment protéger les données numériques sur les réseaux sociaux ?
* Un robot peut-il apprendre comme un être humain ?
