# Questions Grand Oral NSI : Histoire de L'informatique

* Femmes et numérique : quelle histoire ? quel avenir ?
* Ada Lovelace : pionnière du langage informatique
* Alan Turing : la naissance de l'informatique moderne ?
* Quelle est la différence entre le web 1.0 et le web 2.0 ?
* Quels sont les enjeux de l'Intelligence Artificielle ?