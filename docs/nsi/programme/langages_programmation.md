## Questions Grand Oral NSI : Langages et Programmation

* P = NP, un problème à un million de dollars ?
* Tours de Hanoï : plus qu’un jeu d’enfants ?
* Les fractales : informatique et mathématiques imitent-elles la nature ?
* De la récurrence à la récursivité
* Les bugs : bête noire des développeurs ?
* Comment rendre l’informatique plus sûre ?