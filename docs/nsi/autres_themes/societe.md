# Questions Grand Oral NSI : Informatique & Société

* Quelle est l’empreinte carbone du numérique en termes de consommation ?
* Pourquoi chiffrer ses communications ?
* Les réseaux sociaux sont-ils compatibles avec la politique ?
* Les réseaux sociaux sont-ils compatibles avec le journalisme ?
* Les réseaux sociaux permettent-ils de lutter contre les infox ?
* L’informatique va-t-elle révolutionner la médecine ?
* L’informatique va-t-elle révolutionner la physique ?
* L’informatique va-t-elle révolutionner l’entreprise ?