# Questions Grand Oral NSI : Éthique & Informatique

* Comment protéger les données numériques sur les réseaux sociaux ?
* Pourquoi chiffrer ses communications ?

* Le numérique : facteur de démocratisation ou de fractures sociales ?
* Informatique : quel impact sur le climat ?
