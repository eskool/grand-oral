# Aménagements de l'Épreuve du Grand Oral

Les candidats à l'examen du baccalauréat général ou technologique dont les troubles impactent la passation de l'épreuve orale terminale (troubles neurodéveloppementaux, troubles du langage oral ou de la parole, troubles des fonctions auditives, troubles psychiques, troubles des fonctions motrices ou maladies invalidantes, etc.) qui souhaitent bénéficier d'aménagements de l'épreuve orale terminale peuvent en faire une demande selon les procédures en vigueur.

Les demandes d'adaptation ou d'aménagements peuvent porter particulièrement sur :

1) Une majoration du temps de préparation ou du temps de passation de l'épreuve ;

2) Une brève pause en raison de la fatigabilité de certains candidats (déductible du temps de passation) ;

3) Une accessibilité des locaux et une installation spécifique de la salle ;

4) Des aides techniques ou du matériel apportés par le candidat ou fournis par l'établissement : utilisation d'une tablette ou d'un ordinateur équipé d'un logiciel spécifique le cas échéant (logiciel de retour vocal par exemple) que l'élève est habitué à utiliser en classe, mais vidé de ses dossiers ou fichiers et hors connexion ;

5) La communication : le port, par au moins un membre du jury, d'un micro haute fréquence (HF), une énonciation claire et simple des questions en face du candidat afin de faciliter une lecture labiale le cas échéant ou toute autre modalité d'adaptation ;

6) Les aides humaines :

* un secrétaire reformulant une question ou expliquant un sens second ou métaphorique, rassurant le candidat ou apportant toute autre aide requise ;
* un enseignant spécialisé dans les troubles des fonctions auditives le cas échéant ;
* un interprète en langue des signes française (LSF) ou un codeur en langage parlé complété (LPC).

7) D'autres adaptations possibles :

* fournir une transcription écrite (avec ou sans aide humaine) pour la présentation orale de la question et pour l'échange sur le projet d'orientation du candidat ;
* répondre par écrits brefs (avec ou sans aide humaine) lors des échanges avec le jury ;
* la présence dans les membres du jury d'au moins une personne maîtrisant la LSF ou le code LPC, le cas échéant, sera préférée à la présence d'un interprète ou d'un codeur ;
* toute autre mesure favorisant les échanges avec le jury et conforme à la réglementation en vigueur.

La grille d'évaluation indicative ci-jointe en annexe 1 doit être prise en compte également pour les élèves à besoins éducatifs particuliers. Le jury veillera à adopter une attitude bienveillante et ouverte afin de permettre d'évaluer les objectifs de l'épreuve dans le respect des compétences spécifiques du candidat.